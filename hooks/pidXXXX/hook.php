<?php
require_once __DIR__ . '/../../vendor/autoload.php';

use CTSI\Hooks\FlintReg\Survey\FlintRegSurveyUtil;
use CTSI\Hooks\FlintReg\Survey\FlintRegVarCollapser;

define('SYNC_MODE', 'production');  //this makes sure the production version of the Flint Registry constants are used, sandbox is default
use CTSI\EPIC\HurleySync;

function redcap_save_record($project_id, $record, $instrument, $event_id, $group_id, $survey_hash, $response_id) {
    $hurley_sync = new HurleySync();
    //execute presave to the changes in the save hook can be detected
    $hurley_sync->FlintRegistryPreSave($project_id, $record);
    // run the save hook logic
    FlintRegSurveyUtil::FlintRegistrySaveHook($project_id, $record, $instrument, $event_id, $group_id, $survey_hash, $response_id);
    $hurley_sync->FlintRegistrySaveHook($project_id, $record, $instrument);
}
