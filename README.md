# Epic HL7 integration

This project contains code that was developed starting in 2019 to support a public health registry that needed to communicate with a local health system's Epic instance.  They wanted to allow an individual to register through a survey and provide annual follow-up surveys.  It was decided that Epic was not an appropriate tool because it required accounts for users, it was associated with entities that had lost trust with the target population, and it was slow to customize.  REDCap was chosen as an alternative way to deliver surveys, but to integrate with Epic to gain the functionality it offers.

## Getting started

This code is designed to fit within the REDCap plugin and hook structure.  At the time of initial development, we had not migrated custom development to the external module framework.  The provided code requires an existing REDCap instance.  This is purely example code and it is not intended to run in its current form. It must be modified to fit your environment.

## Usage

### Dependencies

The project is made up of a few classes included in `\plugins\epic_integration\lib`.  Dependency management was done using composer and the following line enabled the classes that were part of the plugin.

```
require_once __DIR__ . '/../../vendor/autoload.php';
```

### Execution

The included code can execute in two different ways.  The first is through the REDCap hook functionality which is used to add functionality when a record is saved.  This is an old approach that has been replaced by external modules, but the *redcap_save_record* functionality can be found in the `\hooks` directory.

The second method of execution is through a cronjob that runs every two minutes.  This job will find records that need to be queued for sending over the interface and send queued messages.

## Logical Structure

The code uses a secondary REDCap project to serve as a queue for messages and to function as a complete historical record of all the messages that are sent over the interface.  Connections to the HL7 receiver slowed down the execution of `redcap_save_record`, so the message queue project allowed the code to be split into two different processes in order to be efficient.  One writes to the queue (fast, in real time) while the second sends queued messages (slow, batch job). The [data dictionary](dd/HL7Log_DataDictionary.csv) for this supplementary project has been included.  We are approaching a half million messages sent through the interface, so the message queue project has been split into a recent and an archive version to maintain performance of current messages, but allow for a full log history.

It was also necessary to force a record to update, rather then only detecting a change created by the registry specify save hook.  The field **force_hl7_update** was used to indicate the record should be sent and is often set on a data import from a third party source.  Every time a record is sent, this field was set back to false.

Example HL7 messages are included in [exampleADT](exampleADT.md) and [exampleFlowSheet](exampleFlowSheet.md).

## Authors and acknowledgment
Thanks to Morgan Edwards and Joe Grathoff at Hurley Medical center for their assistance in defining the details of the HL7 message structure, debugging, and testing the various releases of this code.  Thanks to Mike Szeidel at Michigan State University for his work on the other aspects of the registry.  His structure for handling record saves made it easy to add interface updates when data change.

This work was supported by grant funding from the Centers for Disease Control and Prevention (CDC) of the U.S. Department of Health and Human Services (HHS) (NUE2EH001370) as part of an award totaling $20,360,339 with 0% financed with non-governmental sources; and by funds from the U.S. Department of Health and Human Services (HHS) (U05M15ADM, FAIN 1705MI5ADM) passed through the Michigan Department of Health and Human Services (E20172805-00 and E20180329-00) as part of awards totaling $193,521.72 with 0% financed with non-governmental sources, and $186.336.16 with 0% financed with non-governmental sources. The contents are those of the authors and do not necessarily represent the official views of, nor an endorsement by, CDC, HHS, the U.S. Government, or the Michigan Department of Health and Human Services.

## License
This code is made available under the MIT license.

## Project status
This project is in a maintenance phase.  Occasionally there are additional OBX parameters added to the interface.  If there is substantial interest or if there is new set of requirements, we would rewrite the code as an external module allowing for easier sharing and detect changes through querying the log event table.


