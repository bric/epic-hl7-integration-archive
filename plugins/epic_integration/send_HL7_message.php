<?php
/**
 * Script that was run via a project bookmark that would force a send of an HL7 update for one individal record.
 * This would only run when there was a currently selected record
 */
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ ."/../../redcap_connect.php";

define('SYNC_MODE', 'production');  //this makes sure the production version of the Flint Registry constants are used, sandbox is default
use CTSI\Flexr\HurleySync;

$hurley_sync = new HurleySync();

require_once APP_PATH_DOCROOT . 'ProjectGeneral/header.php';

## Your HTML page content goes here
$ur = REDCap::getUserRights(USERID);

if(!(SUPER_USER || $ur[USERID]['role_name'] == 'FR Staff' || $ur[USERID]['role_name'] == 'BRIC PC')){
    echo "You do not have permission to run this plugin.";
}
else if (!isset($_GET['record'])) {
    echo "You must have a record in context to run this plugin.";
}
else if($_GET['pid'] != TARGET_PROJECT_ID) {
    echo "You can only run this on the ". SYNC_MODE . " environment.";
}
else {

  //fire the save hook with no presave.  This will ensure the flow data has changed and will be sent
  $hurley_sync->FlintRegistrySaveHook(TARGET_PROJECT_ID, $_GET['record']);
  echo "The message has been sent.";

}

require_once APP_PATH_DOCROOT . 'ProjectGeneral/footer.php';
