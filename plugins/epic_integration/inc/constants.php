<?php

namespace CTSI\EPIC;
if(!defined('SYNC_MODE')){
  define('SYNC_MODE', 'sandbox');
}

if(SYNC_MODE == 'production') {
  log_ts("production");
  define('SOURCE_PROJECT_ID', 193); 
  define('TARGET_PROJECT_ID', 221); 

  // Hurley HL7 Integration
  define('HURLEY_HL7_HOST', '192.0.0.0');
  define('HURLEY_ADT_PORT', 15400);
  define('HURLEY_FLOW_PORT', 15401);
  define('HURLEY_SERVER_TYPE', 'P');  //Production
  define('HURLEY_MESSAGE_LOG_PROJECT_ID', 266); 
}
else {
  log_ts("sandbox");
  define('SOURCE_PROJECT_ID', 186); 
  define('TARGET_PROJECT_ID', 187); 

  // Hurley HL7 POC Integration
  define('HURLEY_HL7_HOST', '192.0.0.1');
  define('HURLEY_ADT_PORT', 10700);
  define('HURLEY_FLOW_PORT', 10701);
  define('HURLEY_SERVER_TYPE', 'T');  //Testing
  define('HURLEY_MESSAGE_LOG_PROJECT_ID', 255);
}

define("BASELINE_EVENT", 'preenroll_data_arm_1');




?>
