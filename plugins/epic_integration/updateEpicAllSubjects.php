<?php

/**
 * Script called every 5 minutes to check for the first 500 records that are forced to update and call the update.
 * This adds the messges to the log of messages to send.  Finally, sendLog with send all waiting messages over the interface.
 * 
 */

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/init.php';
if(!defined('SYNC_MODE')){
  DEFINE('SYNC_MODE', 'production');
}
require_once __DIR__ . '/inc/constants.php';
$_GET['pid'] = TARGET_PROJECT_ID; //this is needed because REDCap::getFieldNames needs to have a project set
$_POST['pid'] = TARGET_PROJECT_ID;

DEFINE("NOAUTH", true);
require_once __DIR__ . '/../../redcap_connect.php';

use CTSI\Flexr\HurleySync;

$hurley_sync = new HurleySync();

$filter = "([force_hl7_update]='1')"; 
$recs = json_decode(REDCap::getData($project_id, 'json', null, REDCap::getFieldNames(array('individual_demographics', 'data_to_transcribe_to_epic', 'communications', 'year_1_tracking','invitation_tracker', 'pulse_and_wave_tracking')), BASELINE_EVENT,
null, false, false, false, $filter));

$records = 0;
foreach($recs as $record){
  //this record was flagged as needing to be sent, so FlintRecordUpdate sends the HL7 and marks it as no longer needing the HL7 update
  $records++;
  if($records<=500)
  	$hurley_sync->FlintRecordUpdate(TARGET_PROJECT_ID, $record);
}
//send the newly queued records, as well as all the records that have not already been sent
$hurley_sync->sendLog();

?>
