<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/init.php';
if(!defined('SYNC_MODE')){
  DEFINE('SYNC_MODE', 'sandbox');
}
require_once __DIR__ . '/updateEpicAllSubjects.php';