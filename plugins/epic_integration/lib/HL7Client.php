<?php

namespace CTSI\EPIC;
use REDCap;

class obxParameter {  //Class to hold the parameters of an OBX line in Epic and format the row appropriately for a record
	private $epicNumber;
	private $fieldName;
	private $dateFormat;
	private $lookupArray;

	public function __construct($num, $field,  $lookupArray = null, $format = null){
		$this->epicNumber = $num;
		$this->fieldName = $field;
		$this->lookupArray = $lookupArray;
		$this->dateFormat = $format;
	}

	private function getValue($record){
		if(!property_exists($record, $this->fieldName)){
			return '';  //if the value in the record is not defined, return empty
		}
		$val =  $record->{$this->fieldName};
		if($val === ''){
			return ''; //OBX lines can't be empty, so if there is no value it should not have  a line
		}
		if($this->dateFormat){
			return date($this->dateFormat, HL7Client::emptystrtotime($val));
		}
		elseif ($this->lookupArray) {
			return $this->lookupArray[$val];
		}
		else {
			return $val;
		}
	}

	public function formatObx($i, $record){
		$val = $this->getValue($record);
		if ($val != '') {
			return "OBX|$i|ST|".$this->epicNumber."||".$val."|||||||||<time>\r";
		}
		return '';
	}

	public function getFieldName() {
		return $this->fieldName;
	}

}

/**
 *
 * This class is designed to manage either ADT for Flowsheet messages.
 * It is purely the conversion of a record into a HL7 messages and sending the message to a HL7 receiver.
 * It does not manage the queue or messages that need to be sent, rather only receives a record to send
 * 
 * 
 * The public methods are:
 * send
 * getFields
 * emptystrtotime (used in OBXParameter)
 *  
 */
class HL7Client
{
	//This class is designed to manage either ADT for Flowsheet messages

	private $host;
	private $port;
	private $socket;
	private $errnum,$errstr;
	private $timeout=2;
	private $attempts = 0;
	private $attempt_limit=20;
	private $formatMethod;  //the name of the function that should be called when formatting the message to be sent to Epic
	private $transscribe_variables;


	public function __construct($host, $port, $format){
		$this->host = $host;
		$this->port = $port;


		if($format == 'ADT'){
			$this->formatMethod = 'formatADTMessage';
		}
		else {
			$this->formatMethod = 'formatFlowMessage';
			//array of all the varaibles that can be sent as OBX parameters, their Epic control numbers, and formatting details
			$this->transscribe_variables = array(
				new obxParameter(13768, 'eligibility_mode', self::$eligibility_mode_lookup),
				new obxParameter(13670, 'dt_eligiblesrvy_submit', null, 'm/d/Y'),
				new obxParameter(14197, 'age_at_eligible'),
				new obxParameter(13727, 'eligibility_status', self::$eligibility_status_lookup),
				new obxParameter(14620, 'dt_consent_join', null, 'm/d/Y'),
				new obxParameter(13767, 'consent_mode', self::$consent_mode_lookup),
				new obxParameter(13728, 'consent_join', self::$consent_status_lookup),
				new obxParameter(13695, 'reason_notconsent', self::$nonconsent_lookup),
				new obxParameter(13724, 'notconsent_otherrsn'),
				new obxParameter(13691, 'basesrvy_mode_reqst', self::$baseline_mode_rqst_lookup),
				new obxParameter(14619, 'basesrvy_mode_use', self::$baseline_mode_use_lookup),
				new obxParameter(13701, 'dt_basesrvy_submit', null, 'm/d/Y'),
				new obxParameter(15098, 'ts', null, 'm/d/Y')
			);
			
			$this->transscribe_variables[] = new obxParameter(7989, 'age_at_oney');
			$this->transscribe_variables[] = new obxParameter(7930, 'oneysrvy_mode_reqst', self::$baseline_mode_rqst_lookup);
			$this->transscribe_variables[] = new obxParameter(7938, 'oneysrvy_mode_use', self::$oney_mode_use_lookup);
			$this->transscribe_variables[] = new obxParameter(7940, 'dt_oneysrvy_submit', null, 'm/d/Y');
		
			$this->transscribe_variables[] = new obxParameter(51013767, 'consent_medicaid_reqst', self::$medicaid_mode_rqst_lookup);
			$this->transscribe_variables[] = new obxParameter(10196, 'consent_medicaid_mode', self::$medicaid_mode_rqst_lookup);
			$this->transscribe_variables[] = new obxParameter(10201, 'consent_decision5', self::$yes_no_lookup);
			$this->transscribe_variables[] = new obxParameter(7936, 'dt_consent_decision5', null, 'm/d/Y');
		
			$this->transscribe_variables[] = new obxParameter(13440, 'dt_bl_sms_sent', null, 'm/d/Y');
			$this->transscribe_variables[] = new obxParameter(13441, 'dt_bl_email_sent', null, 'm/d/Y');
			$this->transscribe_variables[] = new obxParameter(13442, 'dt_oney_sms_sent', null, 'm/d/Y');
			$this->transscribe_variables[] = new obxParameter(13443, 'dt_oney_email_sent', null, 'm/d/Y');
			
			$this->transscribe_variables[] = new obxParameter(18229, 'bcwater', self::$bcwater_lookup);
			$this->transscribe_variables[] = new obxParameter(18230, 'bll', self::$bll_lookup);
			$this->transscribe_variables[] = new obxParameter(18231, 'blldt', self::$blldt_lookup);
			$this->transscribe_variables[] = new obxParameter(24758, 'bll2023_new_scale', self::$bll_lookup_2023);
			
			$this->transscribe_variables[] = new obxParameter(19468, 'adult_bsln', self::$complete_lookup);
			$this->transscribe_variables[] = new obxParameter(19469, 'child_bsln', self::$complete_lookup);
			$this->transscribe_variables[] = new obxParameter(19470, 'adult_oys', self::$complete_lookup);
			$this->transscribe_variables[] = new obxParameter(19471, 'child_oys', self::$complete_lookup);
			$this->transscribe_variables[] = new obxParameter(19956, 'fwcless6', self::$yes_no_missing_lookup);
			$this->transscribe_variables[] = new obxParameter(19957, 'fwcless18', self::$yes_no_missing_lookup);
			$this->transscribe_variables[] = new obxParameter(19958, 'fwcres', self::$yes_no_missing_lookup);
			$this->transscribe_variables[] = new obxParameter(21684, 'verified', self::$yes_no_missing_lookup);

			$this->transscribe_variables[] = new obxParameter(24430, 'age_at_p2301');
			$this->transscribe_variables[] = new obxParameter(24431, 'dt_p2301_submit', null, 'm/d/Y');
			$this->transscribe_variables[] = new obxParameter(24432, 'p2301_mode_reqst', self::$baseline_mode_rqst_lookup);
			$this->transscribe_variables[] = new obxParameter(24433, 'p2301_mode_use', self::$oney_mode_use_lookup);
			$this->transscribe_variables[] = new obxParameter(24434, 'dt_p2301_sms_sent', null, 'm/d/Y');
			$this->transscribe_variables[] = new obxParameter(24435, 'dt_p2301_email_sent', null, 'm/d/Y');

			if(SYNC_MODE != 'production'){
				//retaining for future variables that are only being tested in sandbox
				
				
			}
			
		}

	}
	
	private function connect() {
		//open a socket with the HL7 receiver
		while(!is_resource($this->socket)){
			$this->socket = fsockopen($this->host,$this->port, $this->errnum, $this->errstr, $this->timeout);
			$this->attempts++;
			if(is_resource($this->socket) || $this->attempts >= $this->attempt_limit ){
				break;
			}

			sleep($this->timeout);

		}
		stream_set_timeout($this->socket, 2);
	}

	public function send($record) {
		//each record is sent as a seperate message
		$this->connect();
		$msg = $this->{$this->formatMethod}($record);  //use the formatMethod to determine the function to format the record
		if (!is_resource($this->socket)) {
		    return array('success'=> 0, 'sent'=>$msg, 'error' => "connection fail: ".$this->errnum." ".$this->errstr . "\nattempts: ".$this->attempts);
		} else {

			fwrite($this->socket, $this->mllpWrap($msg));

			$res = '';
			while (!feof($this->socket)) {
				$char = fgets($this->socket, 2);
				$res .= $char;
				if($char == chr(28)){  //mllp messages end with char28 and then 13.  After a 28, read the 13 and that makes a full message
			    	$res .= fgets($this->socket, 2);
 					break;
			 	}
		}
		$ret = array('success'=>1, 'sent'=>$msg,  'response' => $this->mllpUnwrap($res));
		if($this->attempts > 1){  //if more than one attempt was needed, that can reported as an error, even though the message succeeded in sending
			$ret['error'] = "attempts: ".$this->attempts;
		}
		return $ret;
	}
}
	public function getFields(){  //get an array of all the variables that are being sent in the OBX fields
		$fields = array();
		foreach ($this->transscribe_variables as $param) {
			$fields[] = $param->getFieldName();
		}
		return $fields;
	}


	private function formatADTMessage($record) {  //called when $this->formatMethod = 'formatADTMessage'

		$map = $this->mapRecord($record);

		return str_replace(array_keys($map), $map, self::$adt_template);  //plug the record values into the template

	}

	private function formatFlowMessage($record) { //called when $this->formatMethod = 'formatFlowMessage'

		$map = $this->mapRecord($record);
		$msg = str_replace('<obx>', $this->generateOBX($record), self::$flow_sheet_template); 

		return str_replace(array_keys($map), $map, $msg);  //plug the record values into the template

	}


	private function mapRecord($record){
		//map the values from a record onto the template strings.  These will be used to plug the values into the templates
		$map['<app_name>'] = "FLINTREG";
		$map['<org_name>'] = 'FLINTREGISTRY';
		$map['<time>'] = isset($record->save_ts)?$record->save_ts:date('YmdHis');
		$map['<message_id>'] = $record->message_id;
		$map['<server_type>'] = HURLEY_SERVER_TYPE;
		$map['<frid>'] = $record->frid;
		$map['<survey_queue>'] = $record->sq_code; 
		$map['<last_first_middle_name>'] = \strtoupper($record->last_name . "^" . $record->first_name . ($record->middle_name != ''?"^".$record->middle_name:''));
		$map['<dob>'] = ($record->dob != '')?date("Ymd", $this->emptystrtotime($record->dob)):'';
		$map['<sex>'] = self::$gender_lookup[$record->gender];
		$map['<addr>'] = \strtoupper($record->address . ($record->apt_num == '' ? '': ' Apt. ') . $record->apt_num) ;
		$map['<city>'] = \strtoupper($record->city);
		$map['<state>'] = self::$state_lookup[$record->state];
		$map['<zip>'] = $record->zip_code;
		$map['<phone_1>'] = $record->phone_number;
		$map['<email>'] = $record->email_address;
		$map['<area_code_1>'] = $this->area_code($record->phone_number);
		$map['<num_1>'] =  $this->no_area_code($record->phone_number);
		$map['<phone_2>'] = $record->phone2;
		$map['<area_code_2>'] =  $this->area_code($record->phone2);
		$map['<num_2>'] =  $this->no_area_code($record->phone2);
		$map['<name_source>']= $record->name_source;
		$map['<death>'] = 'N';  //All participants are considered alive at enrollment
		$map['<group_id>']= $record->grp;
		$map['<flow_comment>'] = '';

		return $map;
	}

	private function generateOBX($record){
		//each OBX line needs to have its own number, so when a nonempty line exists, increment the line counter
		$i = 0;
		$obx_str = '';
		foreach($this->transscribe_variables as $ind=>$param) {
			$obx = $param->formatObx($i+1, $record);
			if($obx != ''){
				$obx_str .= $obx;
				$i++;
			}
		}
		return $obx_str;
	}


	public static function emptystrtotime($str){
		if($str == ''){
			return '';
		}
		return strtotime($str);

	}

	private function area_code($phone){
		$num = preg_replace("/[^0-9]/", "", $phone );
		return \substr($num, 0, 3);

	}

	private function no_area_code($phone) {
		$num = preg_replace("/[^0-9]/", "", $phone );
		return \substr($num, 3);
	}

	private function mllpWrap($data){
		return chr(11).$data.chr(28).chr(13);
	}

	private function mllpUnwrap($data) {
			if(substr($data, 0, 1) !== chr(11))
					throw new \InvalidArgumentException('Envelope does not start with <VT> (ASCII 11)');

			if(substr($data, -2) !== chr(28).chr(13))
					throw new \InvalidArgumentException('Envelope does not end with <FS><CR> (ASCII 28, 13)');
			return substr($data, 1, -2);
	}

	//Lookup arrays of to go from the coded values to the text strings that Epic is expecting

private static $gender_lookup = array( 1=> 'M', 2=>'F', 3=>'OTH', 99=>'U');
private static $state_lookup = array( 1=> 'MI',
	2 =>'AL',
	3 =>'AK',
	4 =>'AZ',
	5 =>'AR',
	6 =>'CA',
	7 =>'CO',
	8 =>'CT',
	9 =>'DE',
	10 =>'FL',
	11 =>'GA',
	12 =>'HI',
	13 =>'ID',
	14 =>'IL',
	15 =>'IN',
	16 =>'IA',
	17 =>'KS',
	18 =>'KY',
	19 =>'LA',
	20 =>'ME',
	21 =>'MD',
	22 =>'MA',
	23 =>'MN',
	24 =>'MS',
	25 =>'MO',
	26 =>'MT',
	27 =>'NE',
	28 =>'NV',
	29 =>'NH',
	30 =>'NJ',
	31 =>'NM',
	32 =>'NY',
	33 =>'NC',
	34 =>'ND',
	35 =>'OH',
	36 =>'OK',
	37 =>'OR',
	38 =>'PA',
	39 =>'RI',
	40 =>'SC',
	41 =>'SD',
	42 =>'TN',
	43 =>'TX',
	44 =>'UT',
	45 =>'VT',
	46 =>'VA',
	47 =>'WA',
	48 =>'WV',
	49 =>'WI',
	50 =>'WY');

private static $eligibility_mode_lookup = array( 1=> 'Over the Phone, by FR Staff',
	2=> 'In Person (Face-to-Face), by FR Staff',
	4=> 'Online Survey, by Participant (survey code)',
	5=> 'Multiple');
private static $eligibility_status_lookup = array( 1=> "Eligible (ELG)", 0=>"Not Eligible (NT-ELG)");
private static $consent_mode_lookup = array( 1=> 'Over the phone, by FR Staff',  //note lower case phone
	2=> 'In Person (Face-to-Face), by FR Staff',
	3=> 'Paper Survey, sent by Mail',
	4=> 'Online Survey, by Participant (survey code)',
	5=> 'Multiple');
private static $consent_status_lookup = array( 1=> "Consented (CNT)", 0=>"Did not consent (DCL)");
private static $nonconsent_lookup = array(1=> "No reason given",
	2=> "Concerns about data privacy/security",
	3=> "Takes too much time",
	4=> "No value for me/Nothing to gain",
	5=> "Other");
private static $baseline_mode_rqst_lookup = array( 1=> "Telephone", 2=> "In-person", 3=>"Mail", 4=>"Online");
private static $baseline_mode_use_lookup= array( 1=> 'Over the phone, by FR Staff',
	2=> 'In Person (Face-to-Face), by FR Staff',
	3=> "Paper Survey, sent by Mail",
	4=> 'Online Survey, by Participant (survey code)',
	5=> 'Multiple');
	
private static $oney_mode_use_lookup= array( 1=> 'Over the phone, by FR Staff',
	2=> 'In Person (aka Face-to-Face), by FR Staff',
	3=> "Paper-edition Survey, sent by Mail",
	4=> 'Online-edition Survey, by Participant (using survey code)',
	5=> 'Multiple');
	
private static $bcwater_lookup = array(0=>"No", 1=>"Yes", 9=>"Missing");
private static $bll_lookup = array(0=>"Not detected", 1=>"Low <5", 2=>"Medium 5 to <15", 3=>"High 15 to <45", 4=>"Very High 45+", 9=>"Missing");
private static $blldt_lookup = array(1=>"Prior to Nov 2017", 2=>"After Oct 2017", 9=>"Missing");
private static $bll_lookup_2023 = array(0=>"Not detected", 1=>"Low <3.5", 2=>"Medium 3.5 to 19", 3=>"High 20 to 44", 4=>"Very High 45+", 9=>"Missing");

private static $medicaid_mode_rqst_lookup = array( 1=> "Paper", 2=>"Online");
private static $yes_no_lookup = array( 1=> "Yes", 0=>"No");
private static $yes_no_missing_lookup = array( 1=> "Yes", 0=>"No", 2=>'Inconclusive', 9=>'Missing');
private static $complete_lookup = array( 1=> "Complete");

private static $ns_lookup = array(1=>"Pre-Enroll S", 2=>'SOS', 3=>'ABS', 4=>'MCIR', 5=>'HRLY', 6=>'CRIM', 7=>'GFHC', 8=>'GHS');


//templates for the ADT and Flow messages
private static $adt_template = <<<EOF
MSH|^~\&|<app_name>|<org_name>|EPIC|HURLEY|<time>||ADT^A31|<message_id>|<server_type>|2.3||||||\rPID|1||<frid>^^^FRE^FR~<survey_queue>^^^FRS^FS||<last_first_middle_name>||<dob>|<sex>|||<addr>^^<city>^<state>^<zip>^USA^P||<phone_1>^P^PRN^<email>^^<area_code_1>^<num_1>|<phone_2>^P^PRN^^^<area_code_2>^<num_2>||||||||||||||||<death>\rPV2|||||||<name_source>\rNTE|||Optional Comment.\rZPD||||||||||||||<group_id>\r
EOF;


private static $flow_sheet_template = <<<EOF
MSH|^~\&|<app_name>|<org_name>|EPIC|HURLEY|<time>||ORU^R01|<message_id>|<server_type>|2.3\rPID|||<frid>^^^FRE^FR~<survey_queue>^^^FRS^FS||<last_first_middle_name>||<dob>|<sex>|||||\rOBR|||||||<time>||\r<obx>NTE|||<flow_comment>\r
EOF;

}
