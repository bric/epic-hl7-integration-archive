<?php

/**
 * Script that was used to flag records for an update for all registrants, but to break up the process into small batches rather than triggering 60K+ messages
 * This file was run by a cronjob that ran every 5 minutes until the update was complete
 */
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/init.php';
if(!defined('SYNC_MODE')){
  DEFINE('SYNC_MODE', 'production');
}
require_once __DIR__ . '/inc/constants.php';
$_GET['pid'] = TARGET_PROJECT_ID; //this is needed because REDCap::getFieldNames needs to have a project set
$_POST['pid'] = TARGET_PROJECT_ID;

DEFINE("NOAUTH", true);
require_once __DIR__ . '/../../redcap_connect.php';

use CTSI\EPIC\HurleySync;

$hurley_sync = new HurleySync();
$update_version = 1.0 ;
$batch_size = 100;

$filter = "([update_version]= '' or [update_version] < ".$update_version.")"; 

$recs = json_decode(REDCap::getData($project_id, 'json', null, array('frid', 'dup'), BASELINE_EVENT,
null, false, false, false, $filter));

$records = 0;
foreach($recs as $record){
  if($records<=$batch_size && $record->dup != 1){
    $record->force_hl7_update = 1;
    $record->update_version = $update_version;
    $update[] = $record;
    $records++;
  }
  	$hurley_sync->FlintRecordUpdate(TARGET_PROJECT_ID, $record);
}
//save the update_version back the the registry project
$res = REDCap::saveData($project_id, 'json', json_encode($update));

var_dump($res);


?>
