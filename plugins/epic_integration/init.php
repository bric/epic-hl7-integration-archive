<?php


function log_ts($action){
  global $php_log_file;

  if(!isset($php_log_file)){
    $php_log_file = fopen('/var/www/html/secdocs/php_logs/flint_restructure_log', 'a');
  }

  if (php_sapi_name() == "cli") {
   $mode = 'cron';
  } else {
    $mode = 'http';
  }

  fwrite($php_log_file, "\n ". date('Y-m-d H:i:s') . " ($mode) -- $action -- ");

}
