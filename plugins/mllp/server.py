import socket
import sys
##
# Python version of stub code to help with testing the sending of HL7 messages.  It will receive the message and send an ACK.
#


# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('localhost', 11785)

print >>sys.stderr, 'starting up on %s port %s' % server_address
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)

while True:
    # Wait for a connection
    print >>sys.stderr, 'waiting for a connection'
    connection, client_address = sock.accept()
    try:
        print >>sys.stderr, 'connection from', client_address

        # Receive the data in small chunks and retransmit it
        while True:
            data = connection.recv(1)
            print >>sys.stderr, 'received "%s"' % data
            if data:
                # print >>sys.stderr, chr(11) + 'ACK' + chr(26) + chr(13) 
                if data == chr(28):
                    data = connection.recv(1)
                    connection.sendall(chr(11) + 'ACK' + chr(28) + chr(13) )
            else:
                print >>sys.stderr, 'no more data from', client_address
                break
            
    finally:
        # Clean up the connection
        connection.close()